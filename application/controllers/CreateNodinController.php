<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class CreateNodinController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //get data dari nodim
    function index_get(){
        $nodim_id = $this->get('nodim_id');
        $limit = $this->get('limit');
        $page  = $this->get('page');
        if ($nodim_id == '') {
          $this->db->distinct();
          $this->db->select('nodin_id');
          $this->db->select('perihal');
          $this->db->select('input_date');
          $this->db->order_by('nodin_id','DESC');
          $getnodim = $this->db->get('t_export_nodin_swap',$limit,$page)->result();
        } else {
            $this->db->where('nodin_id', $nodim_id);
            $getnodim = $this->db->get('t_export_nodin_swap',$limit,$page)->result();
        }
        $this->response($getnodim, 200);
    }
}
?>
