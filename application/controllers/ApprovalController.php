<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ApprovalController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //get data dari no_nodim
    function index_get(){
        $tabel = $this->get('tabel');
        if ($tabel != '') {
            $getnodim = $this->db->count_all($tabel);
        }

        $this->response($getnodim, 200);
    }

    function index_put(){
      $couter = $this->put('counter');
      $status = $this->put('status');
      $cell_name = json_decode('data');
      foreach ($cell_name as $cn) {
        // code...
      }

      if($counter == 0 AND $status == "Approve"){
        $data = array(
          "STATUS" => "Plan Swap"
        );

        $this->db->where("CELL_NAME",$cn->CELL_NAME);
        $this->db->update('t_nodin_swap',$data);
      }else{
        $this->db->where("CELL_NAME",$cn->CELL_NAME);
        $this->db->delete('t_nodin_swap');
      }
    }
}
?>
