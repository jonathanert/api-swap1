<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class ExportNodinController extends REST_Controller {

    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    public function index_get(){
        $nodin_id = $this->get('nodin_id');
        $this->db->where('NODIN_ID',$nodin_id);
        $getnodin = $this->db->get('t_export_nodin_swap')->result();
        $this->response($getnodin, 200);
    }

    public function index_post()
    {

    }

    public function index_put()
    {

    }

}
