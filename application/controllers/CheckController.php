<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class CheckController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //get data dari nodim
    function index_get(){
      $ne_id = $this->get('ne_id');
      $ne_id = preg_replace("/[0-9]/","",substr($ne_id,6,9));
      $this->db->where('EQ_TYPE',$ne_id);
      $getnodim = $this->db->get('LOOKUP_EQUIPMENT_TYPE')->result();
      $this->response($getnodim, 200);
    }
}
?>
