<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class CountController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //get data dari no_nodim
    function index_get(){
        $tabel = $this->get('tabel');
        if ($tabel != '') {
            $getnodim = $this->db->count_all($tabel);
        }

        $this->response($getnodim, 200);        
    }
}
?>