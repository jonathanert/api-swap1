<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class Stylo4GController extends REST_Controller {

    function __construct($config = 'rest') {
        parent::__construct($config);
        $this->load->database();
    }

    //Menampilkan data dafinci-po
    function index_get() {
        $cell_name = $this->get('cell_name');
        $limit = $this->get('limit');
        $page  = $this->get('page');
        if ($cell_name == '') {
            // $this->db->order_by('cell_name','DESC');
            // $kontak = $this->db->get('c_stylo_master_cell_4g',$limit,$page)->result();
            $kontak = $this->db->query("SELECT fort.* FROM c_stylo_master_cell_4g fort LEFT JOIN t_nodin_swap sun
                                      ON fort.CELL_NAME = sun.CELL_NAME
                                      WHERE sun.CELL_NAME IS NULL
                                      ORDER BY fort.CELL_NAME DESC LIMIT ".$limit." OFFSET ".$page."")->result();
        } else {
            // $this->db->where('cell_name', $cell_name);
            // $kontak = $this->db->get('c_stylo_master_cell_4g',$limit,$page)->result();
            $kontak = $this->db->query('SELECT * FROM c_stylo_master_cell_4g fort
                                        WHERE fort.CELL_NAME LIKE "%'.$cell_name.'%"')->result();
        }
        $this->response($kontak, 200);
    }

    function index_post(){
        $posted = $this->input->post();
        $siteids = $this->input->post('checkbox');
        foreach ($siteids as $post) {
          $data = array(
              'NODIN_ID'  => NULL,
              'REGIONAL'  => $posted['REGIONAL'][$post],
              'VENDOR'    => $posted['VENDOR'][$post],
              'CELL_NAME' => $posted['CELL_NAME'][$post],
              'LAC'       => $posted['LAC'][$post],
              'TAC'       => $posted['TAC'][$post],
              'CI'        => $posted['CI'][$post],
              'NE_ID'     => $posted['NE_ID'][$post],
              'SITE_ID'   => $posted['SITE_ID'][$post],
              "BAND"      => "4G",
              'BTS_NAME'  => $posted['BTS_NAME'][$post],
              'FREQUENCY' => $posted['FREQUENCY'][$post],
              'LONGITUDE' => $posted['LONGITUDE'][$post],
              'LATITUDE'  => $posted['LATITUDE'][$post],
              'BSC_NAME'  => $posted['BSC_NAME'][$post],
              'BSC_ID'    => $posted['BSC_ID'][$post],
              'SITE_NAME' => $posted['SITE_NAME'][$post]
          );

          $datas = array(
            'CELL_NAME'		=> $posted['CELL_NAME'][$post],
            'NE_ID_old'   => $posted['NE_ID'][$post],
            'LAC/TAC_old' => $posted['TAC'][$post],
            'CI_old'      => $posted['CI'][$post],
            'BAND'				=> "4G",
            'STATUS'      => 16,
          );

          $insert = $this->db->insert('t_nodin_swap',$data);
          $insert_swap = $this->db->insert('t_plan_neid', $datas);

          if($insert){
              $this->response($data,200);
          } else{
              $this->response(array('status' => 'fail', 502));
          }

          $i++;
        }
    }

    //update data ke nodim
    function index_put(){

        $puted = $this->put();
        $siteid = $this->put('siteid');

        for($i=0; $i < count($siteid); $i++){
            $data = array(
                'siteid'            => $puted['siteid'][$i],
                'ne_id'             => $puted['ne_id'][$i],
                'sector_id'         => $puted['sector_id'][$i],
                'site_name'         => $puted['site_name'][$i],
                'oss_name'          => $puted['oss_name'][$i],
                'kabupaten'         => $puted['kabupaten'][$i],
                'lac'               => $puted['lac'][$i],
                'cell_name'         => $puted['cell_name'][$i],
                'sac'               => $puted['sac'][$i],
                'scrambling_code'   => $puted['scrambling_code'][$i],
                'rnc'               => $puted['rnc'][$i],
                'rnc_id'            => $puted['rnc_id'][$i],
                'rac'               => $puted['rac'][$i],
                'ura_id'            => $puted['ura_id'][$i],
                'mscs_name'         => $puted['mscs_name'][$i],
                'mscs_spc'          => $puted['mscs_spc'][$i],
                'mgw_name'          => $puted['mgw_name'][$i],
                'mgw_spc'           => $puted['mgw_spc'][$i],
                'locno'             => $puted['locno'][$i],
                'poc_pstn'          => $puted['poc_pstn'][$i],
                'time_zone'         => $puted['time_zone'][$i],
                'sow'               => $puted['sow'][$i],
                'longitude'         => $puted['longitude'][$i],
                'latitude'          => $puted['latitude'][$i]
            );

            $this->db->where('siteid',$siteid[$i]);
            $update = $this->db->update('nodin', $data);

            if($update){
                $this->response($data,200);
            }else{
                $this->response(array('status' => 'fail', 502));
            }
        }
    }
}
