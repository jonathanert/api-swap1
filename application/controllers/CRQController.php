<?php

defined('BASEPATH') OR exit('No direct script access allowed');

require APPPATH . '/libraries/REST_Controller.php';
use Restserver\Libraries\REST_Controller;

class CRQController extends REST_Controller {

    function __construct($config = 'rest')
    {
        parent::__construct($config);
        $this->load->database();
    }

    public function index_get()
    {
      $nodin_id = $this->get('nodin_id');
      if($nodin_id == '') {
        $CRQ = $this->db->get('t_nodin_swap')->result();
      }else{
        $this->db->where('NODIN_ID',$nodin_id);
        $CRQ = $this->db->get('t_nodin_swap')->result();
      }

      if($CRQ) {
        $this->response($CRQ,200);
      }else {
        $this->response(array('status' => 'fail',502));
      }
    }

    public function index_post()
    {

    }

    public function index_put(){
      $crq = $this->put('crq');
      $flag_nodin = $this->put('flag_nodin');
      $CELL_NAME = $this->put('cell_name');
      $MOP_ATTACHMENT = $this->put('mop');
      $TIMELINE_ATTACHMENT = $this->put('timeline');
      $MCC = $this->put('mcc');
      $MNC = $this->put('mnc');
      $START_DATE = $this->put('start_date');
      $END_DATE = $this->put('end_date');


      $data = array(
        'CRQ' => $crq,
        'FLAG_NODIN' => $flag_nodin,
        'MOP_ATTACHMENT' => $MOP_ATTACHMENT,
        'TIMELINE_ATTACHMENT' => $TIMELINE_ATTACHMENT,
        'MCC' => $MCC,
        'MNC' => $MNC,
        'EXECUTION_PLAN_START_TIME' => $START_DATE,
        'EXECUTION_PLAN_END_TIME'   => $END_DATE
      );

      $this->db->where('CELL_NAME',$CELL_NAME);
      $response = $this->db->update('t_nodin_swap',$data);

      if($response) {
        $this->response($data,200);
      }else {
        $this->response(array('status' => 'fail',502));
      }
    }

}
